# Resize Image

I was trying to make Telegram stickers and their fixed image length of one side must be 512px and the other equal or less than 512px. Hence I created this script to help me downsize all my image to the correct length

## How to run
1. Ensure the image file directory is created
2. put in all your image in to the image file
3. run the `run.bat`
4. DONE