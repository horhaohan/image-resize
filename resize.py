from PIL import Image
import os 

os.chdir("image") # create a image file 

for each in os.listdir():
    if each.endswith("jpg") or each.endswith("JPG") or each.endswith("png") or each.endswith("PNG"):
        photo = Image.open(each)
        length=photo.size[0]
        width=photo.size[1]

        if length > width:
            ratio = length/512
            new_length=512
            new_width=round(width/ratio)

        else:
            ratio = width/512
            new_width=512
            new_length=round(length/ratio)

        photo= photo.resize((new_length,new_width), Image.ANTIALIAS)
        photo.save('mini' + each, quality =95)